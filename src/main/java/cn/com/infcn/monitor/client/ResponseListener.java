package cn.com.infcn.monitor.client;

import cn.com.infcn.monitor.bean.IFCResponseBean;

public interface ResponseListener {

	public void messageReceived(IFCResponseBean result);

	public void onExceptionCaught(Throwable throwable);

	public void sessionOpened();

	public void sessionClosed();
	
}
