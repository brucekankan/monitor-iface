package cn.com.infcn.monitor.jvm.bean;

import java.io.Serializable;

public class IFCJVMClassLoading implements Serializable{

	private static final long serialVersionUID = 8766803377592834246L;

	private long loadedClassCount;	//返回当前加载到 Java 虚拟机中的类的数量。
	
	private long totalLoadedClassCount;	//返回自 Java 虚拟机开始执行到目前已经加载的类的总数。
	
	private long unloadedClassCount;	//返回自 Java 虚拟机开始执行到目前已经卸载的类的总数。
	
	private boolean isVerbose;	//测试是否已为类加载系统启用了 verbose 输出。

	public long getLoadedClassCount() {
		return loadedClassCount;
	}

	public void setLoadedClassCount(long loadedClassCount) {
		this.loadedClassCount = loadedClassCount;
	}

	public long getTotalLoadedClassCount() {
		return totalLoadedClassCount;
	}

	public void setTotalLoadedClassCount(long totalLoadedClassCount) {
		this.totalLoadedClassCount = totalLoadedClassCount;
	}

	public long getUnloadedClassCount() {
		return unloadedClassCount;
	}

	public void setUnloadedClassCount(long unloadedClassCount) {
		this.unloadedClassCount = unloadedClassCount;
	}

	public boolean getIsVerbose() {
		return isVerbose;
	}

	public void setIsVerbose(boolean isVerbose) {
		this.isVerbose = isVerbose;
	}

	@Override
	public String toString() {
		return "IFCJVMClassLoading [loadedClassCount=" + loadedClassCount
				+ ", totalLoadedClassCount=" + totalLoadedClassCount
				+ ", unloadedClassCount=" + unloadedClassCount + ", isVerbose="
				+ isVerbose + "]";
	}
	
	
}
