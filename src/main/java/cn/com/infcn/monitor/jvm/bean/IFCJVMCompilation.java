package cn.com.infcn.monitor.jvm.bean;

import java.io.Serializable;

public class IFCJVMCompilation implements Serializable {

	private static final long serialVersionUID = -7909718127192983834L;

	private String name; // 返回即时 (JIT) 编译器的名称。

	private long totalCompilationTime; // 返回在编译上花费的累积耗费时间的近似值（以毫秒为单位）。

	private boolean isCompilationTimeMonitoringSupported; // 测试 Java虚拟机是否支持监视编译时间。

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getTotalCompilationTime() {
		return totalCompilationTime;
	}

	public void setTotalCompilationTime(long totalCompilationTime) {
		this.totalCompilationTime = totalCompilationTime;
	}

	public boolean getIsCompilationTimeMonitoringSupported() {
		return isCompilationTimeMonitoringSupported;
	}

	public void setIsCompilationTimeMonitoringSupported(
			boolean isCompilationTimeMonitoringSupported) {
		this.isCompilationTimeMonitoringSupported = isCompilationTimeMonitoringSupported;
	}

	@Override
	public String toString() {
		return "IFCJVMCompilation [name=" + name + ", totalCompilationTime="
				+ totalCompilationTime
				+ ", isCompilationTimeMonitoringSupported="
				+ isCompilationTimeMonitoringSupported + "]";
	}

	
}
