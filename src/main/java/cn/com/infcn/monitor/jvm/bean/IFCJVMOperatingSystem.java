package cn.com.infcn.monitor.jvm.bean;

import java.io.Serializable;

public class IFCJVMOperatingSystem implements Serializable {

	private static final long serialVersionUID = 6231993091659963041L;

	private String arch; // 返回操作系统的架构。
	private int availableProcessors; // 返回 Java 虚拟机可以使用的处理器数目。
	private String name; // 返回操作系统名称。
	private double systemLoadAverage; // 返回最后一分钟内系统加载平均值。
	private String version; // 返回操作系统的版本。

	public String getArch() {
		return arch;
	}

	public void setArch(String arch) {
		this.arch = arch;
	}

	public int getAvailableProcessors() {
		return availableProcessors;
	}

	public void setAvailableProcessors(int availableProcessors) {
		this.availableProcessors = availableProcessors;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSystemLoadAverage() {
		return systemLoadAverage;
	}

	public void setSystemLoadAverage(double systemLoadAverage) {
		this.systemLoadAverage = systemLoadAverage;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "IFCJVMOperatingSystem [arch=" + arch + ", availableProcessors="
				+ availableProcessors + ", name=" + name
				+ ", systemLoadAverage=" + systemLoadAverage + ", version="
				+ version + "]";
	}
	

}
