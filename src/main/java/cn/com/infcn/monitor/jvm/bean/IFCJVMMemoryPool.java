package cn.com.infcn.monitor.jvm.bean;

import java.io.Serializable;

public class IFCJVMMemoryPool implements Serializable {

	private static final long serialVersionUID = -8723159199383310843L;

	private IFCJVMMemoryUsage edenMemoryUsage;	//Eden区 GC信息
	
	private IFCJVMMemoryUsage survivorMemoryUsage;	//Survivor区 GC信息
	
	private IFCJVMMemoryUsage codeCacheMemoryUsage;	//代码缓冲区
	
	private IFCJVMMemoryUsage permGenMemoryUsage;	//Perm Gen区 GC信息
	
	private IFCJVMMemoryUsage oldGenMemoryUsage;	//Old Gen区 GC信息
	
	private IFCJVMMemoryUsage metaspaceMemoryUsage;	//Metaspace区 GC信息

	public IFCJVMMemoryUsage getEdenMemoryUsage() {
		return edenMemoryUsage;
	}

	public void setEdenMemoryUsage(IFCJVMMemoryUsage edenMemoryUsage) {
		this.edenMemoryUsage = edenMemoryUsage;
	}

	public IFCJVMMemoryUsage getSurvivorMemoryUsage() {
		return survivorMemoryUsage;
	}

	public void setSurvivorMemoryUsage(IFCJVMMemoryUsage survivorMemoryUsage) {
		this.survivorMemoryUsage = survivorMemoryUsage;
	}

	public IFCJVMMemoryUsage getCodeCacheMemoryUsage() {
		return codeCacheMemoryUsage;
	}

	public void setCodeCacheMemoryUsage(IFCJVMMemoryUsage codeCacheMemoryUsage) {
		this.codeCacheMemoryUsage = codeCacheMemoryUsage;
	}

	public IFCJVMMemoryUsage getPermGenMemoryUsage() {
		return permGenMemoryUsage;
	}

	public void setPermGenMemoryUsage(IFCJVMMemoryUsage permGenMemoryUsage) {
		this.permGenMemoryUsage = permGenMemoryUsage;
	}

	public IFCJVMMemoryUsage getOldGenMemoryUsage() {
		return oldGenMemoryUsage;
	}

	public void setOldGenMemoryUsage(IFCJVMMemoryUsage oldGenMemoryUsage) {
		this.oldGenMemoryUsage = oldGenMemoryUsage;
	}

	public IFCJVMMemoryUsage getMetaspaceMemoryUsage() {
		return metaspaceMemoryUsage;
	}

	public void setMetaspaceMemoryUsage(IFCJVMMemoryUsage metaspaceMemoryUsage) {
		this.metaspaceMemoryUsage = metaspaceMemoryUsage;
	}

	@Override
	public String toString() {
		
		return "edenMemoryUsage:{"+edenMemoryUsage.toString()+"}\r\n"
				+"survivorMemoryUsage:{"+survivorMemoryUsage.toString()+"}\r\n"
				+"codeCacheMemoryUsage:{"+codeCacheMemoryUsage.toString()+"}\r\n"
				+"permGenMemoryUsage:{"+permGenMemoryUsage.toString()+"}\r\n"
				+"oldGenMemoryUsage:{"+oldGenMemoryUsage.toString()+"}\r\n"
				+"metaspaceMemoryUsage:{"+metaspaceMemoryUsage.toString()+"}";
		
	}
	
	

	// objName:java.lang:name=PS Eden Space,type=MemoryPool
	// name:PS Eden Space
	// usage:init = 4259840(4160K) used = 0(0K) committed = 5046272(4928K) max =
	// 10616832(10368K)
	// objName:java.lang:name=PS Survivor Space,type=MemoryPool
	// name:PS Survivor Space
	// usage:init = 655360(640K) used = 98304(96K) committed = 262144(256K) max
	// = 262144(256K)
	// objName:java.lang:name=Code Cache,type=MemoryPool
	// name:Code Cache
	// usage:null
	// objName:java.lang:name=PS Perm Gen,type=MemoryPool
	// name:PS Perm Gen
	// usage:init = 16777216(16384K) used = 11478360(11209K) committed =
	// 21954560(21440K) max = 67108864(65536K)
	// objName:java.lang:name=PS Old Gen,type=MemoryPool
	// name:PS Old Gen
	// usage:init = 11206656(10944K) used = 2843544(2776K) committed =
	// 11206656(10944K) max = 22413312(21888K)
}
