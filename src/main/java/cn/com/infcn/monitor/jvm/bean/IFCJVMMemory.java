package cn.com.infcn.monitor.jvm.bean;

import java.io.Serializable;

public class IFCJVMMemory implements Serializable {

	private static final long serialVersionUID = -8036258630890628033L;

	private IFCJVMMemoryUsage heapMemoryUsage; // 返回用于对象分配的堆的当前内存使用量。
	
	private IFCJVMMemoryUsage nonHeapMemoryUsage; // 返回 Java 虚拟机使用的非堆内存的当前内存使用量。
	
	private int objectPendingFinalizationCount; // 返回其终止被挂起的对象的近似数目。
	
	private boolean isVerbose; // 测试内存系统的 verbose 输出是否已启用。

	public IFCJVMMemoryUsage getHeapMemoryUsage() {
		return heapMemoryUsage;
	}

	public void setHeapMemoryUsage(IFCJVMMemoryUsage heapMemoryUsage) {
		this.heapMemoryUsage = heapMemoryUsage;
	}

	public IFCJVMMemoryUsage getNonHeapMemoryUsage() {
		return nonHeapMemoryUsage;
	}

	public void setNonHeapMemoryUsage(IFCJVMMemoryUsage nonHeapMemoryUsage) {
		this.nonHeapMemoryUsage = nonHeapMemoryUsage;
	}

	public int getObjectPendingFinalizationCount() {
		return objectPendingFinalizationCount;
	}

	public void setObjectPendingFinalizationCount(
			int objectPendingFinalizationCount) {
		this.objectPendingFinalizationCount = objectPendingFinalizationCount;
	}

	public boolean getIsVerbose() {
		return isVerbose;
	}

	public void setIsVerbose(boolean isVerbose) {
		this.isVerbose = isVerbose;
	}

	@Override
	public String toString() {
		return "IFCJVMMemory [heapMemoryUsage=" + heapMemoryUsage
				+ ", nonHeapMemoryUsage=" + nonHeapMemoryUsage
				+ ", objectPendingFinalizationCount="
				+ objectPendingFinalizationCount + ", isVerbose=" + isVerbose
				+ "]";
	}
	
}
