package cn.com.infcn.monitor.jvm.bean;

import java.io.Serializable;
import java.util.Arrays;

public class IFCJVMMemoryManager implements Serializable {

	private static final long serialVersionUID = -1277650537964226979L;

	private String[] memoryPoolNames; // 返回此内存管理器管理的内存池名称。
	
	private String name; // 返回表示此内存管理器的名称。


	public String[] getMemoryPoolNames() {
		return memoryPoolNames;
	}

	public void setMemoryPoolNames(String[] memoryPoolNames) {
		this.memoryPoolNames = memoryPoolNames;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "IFCJVMMemoryManager [getMemoryPoolNames="
				+ Arrays.toString(memoryPoolNames) + ", name=" + name
				+ "]";
	}
	

}
