package cn.com.infcn.monitor.jvm.bean;

import java.io.Serializable;

public class IFCJVMMemoryUsage implements Serializable {

	private static final long serialVersionUID = -7956018694953885273L;

	private long init; // 返回 Java 虚拟机最初从操作系统请求用于内存管理的内存量（以字节为单位）。

	private long used; // 返回已使用的内存量（以字节为单位）。

	private long committed; // 返回已提交给 Java 虚拟机使用的内存量（以字节为单位）。

	private long max; // 返回可以用于内存管理的最大内存量（以字节为单位）。

	public long getInit() {
		return init;
	}

	public void setInit(long init) {
		this.init = init;
	}

	public long getUsed() {
		return used;
	}

	public void setUsed(long used) {
		this.used = used;
	}

	public long getCommitted() {
		return committed;
	}

	public void setCommitted(long committed) {
		this.committed = committed;
	}

	public long getMax() {
		return max;
	}

	public void setMax(long max) {
		this.max = max;
	}

	@Override
	public String toString() {
		return "IFCJVMMemoryUsage [init=" + init + ", used=" + used
				+ ", committed=" + committed + ", max=" + max + "]";
	}

}
