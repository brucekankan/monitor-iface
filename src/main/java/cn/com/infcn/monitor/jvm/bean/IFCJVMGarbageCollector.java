package cn.com.infcn.monitor.jvm.bean;

import java.io.Serializable;

public class IFCJVMGarbageCollector implements Serializable{

	private static final long serialVersionUID = 5095109363051387571L;
	
	private String name;

	private long collectionCount;	//返回已发生的回收的总次数。
	
	private long collectionTime;	//返回近似的累积回收时间（以毫秒为单位）。

	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getCollectionCount() {
		return collectionCount;
	}

	public void setCollectionCount(long collectionCount) {
		this.collectionCount = collectionCount;
	}

	public long getCollectionTime() {
		return collectionTime;
	}

	public void setCollectionTime(long collectionTime) {
		this.collectionTime = collectionTime;
	}

	@Override
	public String toString() {
		return "IFCJVMGarbageCollector [name=" + name + ", collectionCount="
				+ collectionCount + ", collectionTime=" + collectionTime + "]";
	}
	
	
}
