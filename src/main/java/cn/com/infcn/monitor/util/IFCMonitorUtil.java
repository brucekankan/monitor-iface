package cn.com.infcn.monitor.util;

import java.math.BigDecimal;

public class IFCMonitorUtil {
	// 最大
	private final static long DISK_WR_MAX_VAULE = 1024 * 1024 * 1024 * 4L;

	public static int convertInt(boolean b) {
		if (b) {
			return 1;
		} else {
			return 0;
		}
	}

	/**
	 * 获取一段时间内磁盘IO的读/写值
	 * 
	 * @param before
	 *            第一次获取的读/写值(byte)
	 * @param after
	 *            第二次获取的读/写值(byte)
	 * @return 返回读/写值差(byte)
	 */
	public static long getDiskIO(long before, long after) {
		// 如果after<before说明 after已经大于4GB大小，达到4GB后，从新清零累加
		if (after < before) {
			return (DISK_WR_MAX_VAULE - before) + after;
		} else if (after > before) {
			return after - before;
		} else {
			return 0;
		}
	}

	/**
	 * byte(字节)根据长度转成kb(千字节)和mb(兆字节)
	 * 
	 * @param bytes
	 * @return
	 */
	public static String bytes2Convert(long bytes) {
		BigDecimal filesize = new BigDecimal(bytes);
		BigDecimal gigabyte = new BigDecimal(1024 * 1024 * 1024);
		float returnValue = filesize.divide(gigabyte, 2, BigDecimal.ROUND_UP).floatValue();
		if (returnValue >= 1)
			return (returnValue + "GB");
		
		BigDecimal megabyte = new BigDecimal(1024 * 1024);
		returnValue = filesize.divide(megabyte, 2, BigDecimal.ROUND_UP).floatValue();
		if (returnValue >= 1)
			return (returnValue + "MB");
		
		BigDecimal kilobyte = new BigDecimal(1024);
		returnValue = filesize.divide(kilobyte, 2, BigDecimal.ROUND_UP).floatValue();
		return (returnValue + "KB");
	}
	
	/**
	 * 字节转KB
	 * @param bytes
	 * @return
	 */
	public static float bytes2KB(long bytes) {
		BigDecimal filesize = new BigDecimal(bytes);
		BigDecimal kilobyte = new BigDecimal(1024);
		float returnValue = filesize.divide(kilobyte, 2, BigDecimal.ROUND_UP).floatValue();
		return returnValue;
	}
	
	/**
	 * 字节转MB
	 * @param bytes
	 * @return
	 */
	public static float bytes2MB(long bytes) {
		BigDecimal filesize = new BigDecimal(bytes);
		BigDecimal megabyte = new BigDecimal(1024 * 1024);
		float returnValue = filesize.divide(megabyte, 2, BigDecimal.ROUND_UP).floatValue();
		return returnValue;
	}
	
	/**
	 * 字节转GB
	 * @param bytes
	 * @return
	 */
	public static float bytes2GB(long bytes) {
		BigDecimal filesize = new BigDecimal(bytes);
		BigDecimal megabyte = new BigDecimal(1024 * 1024 * 1024);
		float returnValue = filesize.divide(megabyte, 2, BigDecimal.ROUND_UP).floatValue();
		return returnValue;
	}
	
	public static void main(String[] args) throws Exception{
		System.out.println(bytes2Convert(1024*1024+1));
		float a=bytes2KB(1024*1024+1);
		float b=bytes2GB(1024*1024+1);
		System.out.println(a);
		System.out.println(b);
	}
}
