package cn.com.infcn.monitor.bean;

import java.io.Serializable;

public class IFCUser implements Serializable {

	private static final long serialVersionUID = 6688487565059622580L;

	private String user;	//当前系统进程表中的用户名
	private String device;	//用户控制台
	private String host;	//用户host
	private long time = 0L;

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

}
