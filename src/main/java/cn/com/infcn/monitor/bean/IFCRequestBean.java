package cn.com.infcn.monitor.bean;

import java.io.Serializable;

import cn.com.infcn.monitor.util.MonitorType;

public class IFCRequestBean implements Serializable {
	private static final long serialVersionUID = 7912041191198283523L;

	private MonitorType monitorType;
	private Object params;

	public MonitorType getMonitorType() {
		return monitorType;
	}

	public void setMonitorType(MonitorType monitorType) {
		this.monitorType = monitorType;
	}

	public Object getParams() {
		return params;
	}

	public void setParams(Object params) {
		this.params = params;
	}

}
