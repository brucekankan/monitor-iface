package cn.com.infcn.monitor.bean;

import java.io.Serializable;

public class IFCMem implements Serializable {

	private static final long serialVersionUID = -8376043748105277629L;

	private long total = 0L; // 内存总量
	private long ram = 0L;	//内存大小(MB)
	private long used = 0L; // 当前内存使用量
	private long free = 0L; // 当前内存剩余量
	private long actualUsed = 0L;//真实使用大小
	private long actualFree = 0L;//真实空闲大小
	private double usedPercent = 0.0D;//使用百分比
	private double freePercent = 0.0D;//空闲百分比

	private long swapTotal = 0L; // 交换区总量
	private long swapUsed = 0L; // 当前交换区使用量
	private long swapFree = 0L; // 当前交换区剩余量
	/**
	 * 1、page in/out：系统运行过程中，它一般不会导致严重的性能问题，即使你的物理内存再大，也难免会出现或多或少的PAGE
	 * IN/OUT，这是多任务操作系统的内在机制决定的； 
	 * 2、swap in/out：一般来说是系统物理内存比较紧张的表现，它是PAGE
	 * IN/OUT行为的进一步升级，PAGE IN/OUT是说明系统内存不够用，但仅仅是PAGE或BLOCK和磁盘的交换，但PAGE
	 * IN/OUT这种“内存不够用”一般是很难满足的，也是动态的。 
	 * 3、page in/out和swap in/out的区别是：page
	 * in/out是PAGE 或BLOCK在内存和磁盘间交换，但swap in/out是整个进程空间的交换，这会产生严重的性能问题。
	 * 4、其实，PAGE、SWAP和SGA、PGA没可比性啊，它们应该说有联系，但不能对比着来讨论，前者是操作系统内存管理的一种行为，
	 * 而后者是ORACLE数据库的两个和内存有关的参数，打个不恰当的比方：就象烹饪和饭量；
	 * 5、一般来说，sga和pga是基于操作系统的内存管理机制的，但具体dbms的缓冲内部是怎末管理的
	 * ，我现在不是特别清楚，但我个人认为，DBMS的内存管理和一般软件的内存管理是有区别的
	 * ，这种区别是，DBMS的内存管理不是完全依赖于OS的，也就是DBMS在内存管理方面
	 * ，自主性更强一些，各种DBMS的缓冲区管理机制也不完全相同，个人之见而已，大家一起讨论。 page in/out和swap
	 * in/out是操作系统行为，目的是内存紧张时，空出内存来供给其他进程使用，这和应用软件从磁盘的读入和写出内存不是一回事情，也就是说，
	 * 数据库从磁盘读入数据和写出数据到磁盘的行为，不算是PAGE IN/OUT和SWAP IN/OUT，有关操作系统原理的书籍里，应该有这部分内容。
	 */
	private long swapPageIn = 0L;
	private long swapPageOut = 0L;

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public long getRam() {
		return ram;
	}

	public void setRam(long ram) {
		this.ram = ram;
	}

	public long getUsed() {
		return used;
	}

	public void setUsed(long used) {
		this.used = used;
	}

	public long getFree() {
		return free;
	}

	public void setFree(long free) {
		this.free = free;
	}

	public long getActualUsed() {
		return actualUsed;
	}

	public void setActualUsed(long actualUsed) {
		this.actualUsed = actualUsed;
	}

	public long getActualFree() {
		return actualFree;
	}

	public void setActualFree(long actualFree) {
		this.actualFree = actualFree;
	}

	public double getUsedPercent() {
		return usedPercent;
	}

	public void setUsedPercent(double usedPercent) {
		this.usedPercent = usedPercent;
	}

	public double getFreePercent() {
		return freePercent;
	}

	public void setFreePercent(double freePercent) {
		this.freePercent = freePercent;
	}

	public long getSwapTotal() {
		return swapTotal;
	}

	public void setSwapTotal(long swapTotal) {
		this.swapTotal = swapTotal;
	}

	public long getSwapUsed() {
		return swapUsed;
	}

	public void setSwapUsed(long swapUsed) {
		this.swapUsed = swapUsed;
	}

	public long getSwapFree() {
		return swapFree;
	}

	public void setSwapFree(long swapFree) {
		this.swapFree = swapFree;
	}

	public long getSwapPageIn() {
		return swapPageIn;
	}

	public void setSwapPageIn(long swapPageIn) {
		this.swapPageIn = swapPageIn;
	}

	public long getSwapPageOut() {
		return swapPageOut;
	}

	public void setSwapPageOut(long swapPageOut) {
		this.swapPageOut = swapPageOut;
	}

	public String toString() {
		return "Mem: " + this.total / 1024L + "K av, " + this.used / 1024L + "K used, " + this.free / 1024L + "K free";
	}
}
