package cn.com.infcn.monitor.bean;

import java.io.Serializable;

public class IFCResponseBean implements Serializable {
	private static final long serialVersionUID = 7912041191198283523L;

	private String responseCode;
	private String responseMsg;
	private Object result;

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

}
