package cn.com.infcn.monitor.bean;

import java.io.Serializable;

public class IFCCpu implements Serializable{
	private static final long serialVersionUID = 2254626632502621283L;
	//cpu信息
	private int mhz; // CPU的总量MHz
	private String vendor; // 获得CPU的卖主，如：Intel
	private String model; // 获得CPU的类别，如：Celeron
	private long cacheSize; // 缓冲存储器数量
	private int totalCores;	//CPU核数
	private int totalSockets;	//sockets数
	
	//cpu使用率
	private double user; // CPU用户使用率
	private double sys; // CPU系统使用率
	private double wait; // CPU当前等待率
	private double nice; // CPU当前错误率
	private double idle; // CPU当前空闲率
	private double combined; // CPU总的使用率
	private double irq;	//CPU硬件中断时间
	private double softIrq;	//CPU软件中断时间
	private double stolen;	//CPU使用内部虚拟机运行任务的时间
	
	/*Steal Time（ST）通常还被称作“Stolen CPU”，存在于虚拟计算环境 —— CPU使用内部虚拟机运行任务的时间，由Hypervisor将CPU周期分配给其他“外部任务”产生；而这些外部任务很可能就是你吵闹的邻居（云端共享同一片资源的用户）递交的。
	AWS上的Steal Time
	通过在AWS社区上的研究发现，在CPU达到峰值的一段时间后：系统会自动的将CPU收缩至一定的使用比例，那么你剩下的CPU就被“窃取”了。这种情况通常是云端对自己的保护，以避免崩溃的威胁。*/

	
	
	public int getMhz() {
		return mhz;
	}

	public void setMhz(int mhz) {
		this.mhz = mhz;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public long getCacheSize() {
		return cacheSize;
	}

	public void setCacheSize(long cacheSize) {
		this.cacheSize = cacheSize;
	}

	public int getTotalCores() {
		return totalCores;
	}

	public void setTotalCores(int totalCores) {
		this.totalCores = totalCores;
	}

	public int getTotalSockets() {
		return totalSockets;
	}

	public void setTotalSockets(int totalSockets) {
		this.totalSockets = totalSockets;
	}

	public double getUser() {
		return user;
	}

	public void setUser(double user) {
		this.user = user;
	}

	public double getSys() {
		return sys;
	}

	public void setSys(double sys) {
		this.sys = sys;
	}

	public double getWait() {
		return wait;
	}

	public void setWait(double wait) {
		this.wait = wait;
	}

	public double getNice() {
		return nice;
	}

	public void setNice(double nice) {
		this.nice = nice;
	}

	public double getIdle() {
		return idle;
	}

	public void setIdle(double idle) {
		this.idle = idle;
	}

	public double getCombined() {
		return combined;
	}

	public void setCombined(double combined) {
		this.combined = combined;
	}

	public double getIrq() {
		return irq;
	}

	public void setIrq(double irq) {
		this.irq = irq;
	}

	public double getSoftIrq() {
		return softIrq;
	}

	public void setSoftIrq(double softIrq) {
		this.softIrq = softIrq;
	}

	public double getStolen() {
		return stolen;
	}

	public void setStolen(double stolen) {
		this.stolen = stolen;
	}

	public String toString() {
        return
            "CPU states: " +
            format(this.user) + " user, " +
            format(this.sys)  + " system, " +
            format(this.nice) + " nice, " +
            format(this.wait) + " wait, " +
            format(this.idle) + " idle";
    }
	
	public static String format(double val) {
        String p = String.valueOf(val * 100.0);
        //cant wait for sprintf.
        int ix = p.indexOf(".") + 1;
        String percent =
            p.substring(0, ix) + 
            p.substring(ix, ix+1);
        return percent + "%";
    }
}

