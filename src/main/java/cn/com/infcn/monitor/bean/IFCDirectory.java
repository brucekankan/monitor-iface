package cn.com.infcn.monitor.bean;

import java.io.Serializable;

public class IFCDirectory implements Serializable {

	private static final long serialVersionUID = -4112449724023933496L;

	private String dirPath;	// 目录路径
	private long createDate; // 创建时间
	private long lastAccessTime; // 最后访问时间
	private long size; // 大小
	private long lastModifiedTime; // 最后编辑时间
	private int isSymbolicLink; // 是否是符号连接（文件快捷方式）
	private int isDirectory; // 是否目录
	private int isRegularFile; //是否是普通文件
	private int isHidden; // 是否隐藏
	private int isExists; // 是否存在
	

	public String getDirPath() {
		return dirPath;
	}

	public void setDirPath(String dirPath) {
		this.dirPath = dirPath;
	}

	public long getCreateDate() {
		return createDate;
	}

	public void setCreateDate(long createDate) {
		this.createDate = createDate;
	}

	public long getLastAccessTime() {
		return lastAccessTime;
	}

	public void setLastAccessTime(long lastAccessTime) {
		this.lastAccessTime = lastAccessTime;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public long getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(long lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public int getIsSymbolicLink() {
		return isSymbolicLink;
	}

	public void setIsSymbolicLink(int isSymbolicLink) {
		this.isSymbolicLink = isSymbolicLink;
	}

	public int getIsDirectory() {
		return isDirectory;
	}

	public void setIsDirectory(int isDirectory) {
		this.isDirectory = isDirectory;
	}

	public int getIsRegularFile() {
		return isRegularFile;
	}

	public void setIsRegularFile(int isRegularFile) {
		this.isRegularFile = isRegularFile;
	}

	public int getIsHidden() {
		return isHidden;
	}

	public void setIsHidden(int isHidden) {
		this.isHidden = isHidden;
	}

	public int getIsExists() {
		return isExists;
	}

	public void setIsExists(int isExists) {
		this.isExists = isExists;
	}

}
