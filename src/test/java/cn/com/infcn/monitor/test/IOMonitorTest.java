package cn.com.infcn.monitor.test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import cn.com.infcn.monitor.bean.IFCFileSystem;
import cn.com.infcn.monitor.bean.IFCRequestBean;
import cn.com.infcn.monitor.bean.IFCResponseBean;
import cn.com.infcn.monitor.client.MonitorClientSync;
import cn.com.infcn.monitor.client.ResponseListener;
import cn.com.infcn.monitor.util.IFCMonitorUtil;
import cn.com.infcn.monitor.util.MonitorType;

public class IOMonitorTest {

	public static Map<String, Long> map = new HashMap<String, Long>();

	static class MockListener implements ResponseListener{
		public void messageReceived(IFCResponseBean response) {}
		public void onExceptionCaught(Throwable throwable) {}
		public void sessionOpened() {}
		public void sessionClosed() {}
	}
	
	public static void io(){
		MockListener listener = new MockListener();
		MonitorClientSync client = new MonitorClientSync(listener);
		client.connect("192.168.10.98", 56788);
//		client.connect("192.168.10.105", 56788);
		
		try {
			IFCRequestBean requestBean = new IFCRequestBean();
			requestBean.setMonitorType(MonitorType.FILESYSTEM);
			IFCResponseBean response = client.sendRequest(requestBean);
			IFCFileSystem[] fslist = (IFCFileSystem[])response.getResult();
			
			long readIO = 0;
			long writeIO = 0;
			for (IFCFileSystem fs : fslist) {
				String devName = fs.getDevName();
				String readKey = devName+"-read";
				String writeKey = devName+"-write";
				
				//读IO
				if(map.get(readKey)!=null){
					long beforeRead = map.get(readKey);
					long afterRead = fs.getDiskReadBytes();
					readIO += IFCMonitorUtil.getDiskIO(beforeRead, afterRead);
					map.put(readKey, afterRead);
				}else{
					map.put(readKey, fs.getDiskReadBytes());
				}
				
				//写IO
				if(map.get(writeKey)!=null){
					long beforeWrite = map.get(writeKey);
					long afterWrite = fs.getDiskWriteBytes();
					writeIO += IFCMonitorUtil.getDiskIO(beforeWrite, afterWrite);
					map.put(writeKey, afterWrite);
				}else{
					map.put(writeKey, fs.getDiskWriteBytes());
				}
				
			}
			String r = IFCMonitorUtil.bytes2KB(readIO) +"KB";
			String w = IFCMonitorUtil.bytes2KB(writeIO)+"KB";
			if(r.length()>5){
				System.out.println("读："+ r +"\t 写:"+ w);
			}else{
				System.out.println("读："+ r +"\t\t 写:"+ w);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception {
		while (true) {
			io();
			TimeUnit.SECONDS.sleep(1);
		}
	}
}
