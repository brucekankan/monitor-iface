package cn.com.infcn.monitor.test;

import java.util.Map;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import cn.com.infcn.monitor.bean.IFCCpu;
import cn.com.infcn.monitor.bean.IFCDirectory;
import cn.com.infcn.monitor.bean.IFCFileSystem;
import cn.com.infcn.monitor.bean.IFCMem;
import cn.com.infcn.monitor.bean.IFCNet;
import cn.com.infcn.monitor.bean.IFCOperatingSystem;
import cn.com.infcn.monitor.bean.IFCRequestBean;
import cn.com.infcn.monitor.bean.IFCResponseBean;
import cn.com.infcn.monitor.bean.IFCUser;
import cn.com.infcn.monitor.client.MonitorClientSync;
import cn.com.infcn.monitor.client.ResponseListener;
import cn.com.infcn.monitor.util.MonitorType;


public class MonitorClientSyncTest {

	static MonitorClientSync client;


	static class MockListener implements ResponseListener{

		public void messageReceived(IFCResponseBean response) {
			
		}
		public void onExceptionCaught(Throwable throwable) {
		}

		public void sessionOpened() {
		}

		public void sessionClosed() {
		}
		
	}

	@BeforeClass
	public static void setUp() throws Exception {
		MockListener listener = new MockListener();
		client = new MonitorClientSync(listener);
		client.connect("192.168.10.98", 56788);
//		client.connect("192.168.10.104", 56798);
		client.connect("192.168.3.144", 56788);
	}

	@AfterClass
	public static void tearDown() throws Exception {
		client.disconnect();
	}
	
	public static void main(String[] args) throws Exception{
		MonitorClientSyncTest t = new MonitorClientSyncTest();
		setUp();
//		t.jvmTest();
		t.cpuTest();
//		t.fileSystemTest();
//		t.netTest();
//		t.osTest();
//		t.sysMemTest();
//		t.userTest();
		tearDown();
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void jvmTest(){
		try {
			IFCRequestBean requestBean = new IFCRequestBean();
			requestBean.setMonitorType(MonitorType.JVM);
			IFCResponseBean response = client.sendRequest(requestBean);
			Map<String, Object> map = (Map<String, Object>)response.getResult();
			Set<String> keys = map.keySet();
			for (String key : keys) {
				System.out.println(key+"---"+map.get(key));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void cpuTest(){
		try {
			IFCRequestBean requestBean = new IFCRequestBean();
			requestBean.setMonitorType(MonitorType.CPU);
			IFCResponseBean response = client.sendRequest(requestBean);
			
			IFCCpu[] cpus = (IFCCpu[])response.getResult();
			for (IFCCpu cpu : cpus) {
				System.out.println("CPU的总量MHz:" + cpu.getMhz());// CPU的总量MHz
				System.out.println("CPU生产商:" + cpu.getVendor());// 获得CPU的卖主，如：Intel
				System.out.println("CPU类别:" + cpu.getModel());// 获得CPU的类别，如：Celeron
				System.out.println();
				System.out.println("CPU缓存数量:" + cpu.getCacheSize());// 缓冲存储器数量
				
				System.out.println("CPU用户使用率:" + IFCCpu.format(cpu.getUser()));// 用户使用率
				System.out.println("CPU系统使用率:" + cpu.format(cpu.getSys()));// 系统使用率
				System.out.println("CPU当前等待率:" + cpu.format(cpu.getWait()));// 当前等待率
				System.out.println("改变过优先级的进程的占用CPU的百分比:" + cpu.format(cpu.getNice()));//改变过优先级的进程的占用CPU的百分比
				System.out.println("CPU当前空闲率:" + cpu.format(cpu.getIdle()));// 当前空闲率
				System.out.println("CPU总的使用率:" + cpu.format(cpu.getCombined()));// 总的使用率
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void fileSystemTest(){
		try {
			IFCRequestBean requestBean = new IFCRequestBean();
			requestBean.setMonitorType(MonitorType.FILESYSTEM);
			IFCResponseBean response = client.sendRequest(requestBean);
			IFCFileSystem[] fslist = (IFCFileSystem[])response.getResult();
			
			for (IFCFileSystem fs : fslist) {
				// 分区的盘符名称
				System.out.println("盘符名称:" + fs.getDevName());
				// 分区的盘符名称
				System.out.println("盘符路径:" + fs.getDirName());
				System.out.println("盘符标志:" + fs.getFlags());//
				// 文件系统类型，比如 FAT32、NTFS
				System.out.println("盘符类型:" + fs.getSysTypeName());
				// 文件系统类型名，比如本地硬盘、光驱、网络文件系统等
				System.out.println("盘符类型名:" + fs.getTypeName());
				// 文件系统类型
				System.out.println("盘符文件系统类型:" + fs.getType());
				
				
				// 文件系统总大小
				System.out.println(fs.getDevName() + "总大小:" + fs.getTotal() + "KB");
				// 文件系统剩余大小
				System.out.println(fs.getDevName() + "剩余大小:" + fs.getFree() + "KB");
				// 文件系统可用大小
				System.out.println(fs.getDevName() + "可用大小:" + fs.getAvail() + "KB");
				// 文件系统已经使用量
				System.out.println(fs.getDevName() + "已经使用量:" + fs.getUsed() + "KB");
				double usePercent = fs.getUsePercent() * 100D;
				// 文件系统资源的利用率
				System.out.println(fs.getDevName() + "资源的利用率:" + usePercent + "%");
				
				System.out.println(fs.getDevName() + "读出：" + fs.getDiskReads());
				System.out.println(fs.getDevName() + "写入：" + fs.getDiskWrites());
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void netTest(){
		try {
			IFCRequestBean requestBean = new IFCRequestBean();
			requestBean.setMonitorType(MonitorType.NET);
			IFCResponseBean response = client.sendRequest(requestBean);
			IFCNet[] nets = (IFCNet[])response.getResult();
			for (IFCNet net : nets) {
				System.out.println("网络设备名:" + net.getName());// 网络设备名
				System.out.println("IP地址:" + net.getAddress());// IP地址
				System.out.println("子网掩码:" + net.getNetmask());// 子网掩码
				System.out.println(net.getName() + "接收的总包裹数:" + net.getRxPackets());// 接收的总包裹数
				System.out.println(net.getName() + "发送的总包裹数:" + net.getTxPackets());// 发送的总包裹数
				System.out.println(net.getName() + "接收到的总字节数:" + net.getRxBytes());// 接收到的总字节数
				System.out.println(net.getName() + "发送的总字节数:" + net.getTxBytes());// 发送的总字节数
				System.out.println(net.getName() + "接收到的错误包数:" + net.getRxErrors());// 接收到的错误包数
				System.out.println(net.getName() + "发送数据包时的错误数:" + net.getTxErrors());// 发送数据包时的错误数
				System.out.println(net.getName() + "接收时丢弃的包数:" + net.getRxDropped());// 接收时丢弃的包数
				System.out.println(net.getName() + "发送时丢弃的包数:" + net.getTxDropped());// 发送时丢弃的包数
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void osTest(){
		try {
			IFCRequestBean requestBean = new IFCRequestBean();
			requestBean.setMonitorType(MonitorType.OS);
			IFCResponseBean response = client.sendRequest(requestBean);
			
			IFCOperatingSystem os = (IFCOperatingSystem)response.getResult();
			// 操作系统内核类型如： 386、486、586等x86
			System.out.println("操作系统:" + os.getArch());
			System.out.println("操作系统CpuEndian():" + os.getCpuEndian());//
			System.out.println("操作系统DataModel():" + os.getDataModel());//
			// 系统描述
			System.out.println("操作系统的描述:" + os.getDescription());
			// 操作系统类型
			 System.out.println("OS.getName():"+ os.getName());
			 System.out.println("OS.getPatchLevel():"+ os.getPatchLevel());//
			// 操作系统的卖主
			System.out.println("操作系统的卖主:" + os.getVendor());
			// 卖主名称
			System.out.println("操作系统的卖主名:" + os.getVendorCodeName());
			// 操作系统名称
			System.out.println("操作系统名称:" + os.getVendorName());
			// 操作系统卖主类型
			System.out.println("操作系统卖主类型:" + os.getVendorVersion());
			// 操作系统的版本号
			System.out.println("操作系统的版本号:" + os.getVersion());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void sysMemTest(){
		try {
			IFCRequestBean requestBean = new IFCRequestBean();
			requestBean.setMonitorType(MonitorType.SYSMEM);
			IFCResponseBean response = client.sendRequest(requestBean);
			
			IFCMem mem = (IFCMem)response.getResult();
			// 内存总量
			System.out.println("内存总量:" + mem.getTotal() / 1024L + "K av");
			// 当前内存使用量
			System.out.println("当前内存使用量:" + mem.getUsed() / 1024L + "K used");
			// 当前内存剩余量
			System.out.println("当前内存剩余量:" + mem.getFree() / 1024L + "K free");
			// 交换区总量
			System.out.println("交换区总量:" + mem.getSwapTotal() / 1024L + "K av");
			// 当前交换区使用量
			System.out.println("当前交换区使用量:" + mem.getSwapUsed() / 1024L + "K used");
			// 当前交换区剩余量
			System.out.println("当前交换区剩余量:" + mem.getSwapFree() / 1024L + "K free");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void userTest(){
		try {
			IFCRequestBean requestBean = new IFCRequestBean();
			requestBean.setMonitorType(MonitorType.USER);
			IFCResponseBean response = client.sendRequest(requestBean);
			
			IFCUser[] users = (IFCUser[])response.getResult();
			if (users != null && users.length > 0) {
				for (int i = 0; i < users.length; i++) {
					// System.out.println("当前系统进程表中的用户名"+ String.valueOf(i));
					IFCUser user = users[i];
					System.out.println("用户控制台:" + user.getDevice());
					System.out.println("用户host:" + user.getHost());
					 System.out.println("getTime():"+ user.getTime());
					// 当前系统进程表中的用户名
					System.out.println("当前系统进程表中的用户名:" + user.getUser());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void directoryTest(){
		try {
			IFCRequestBean requestBean = new IFCRequestBean();
			requestBean.setMonitorType(MonitorType.DIRECTORY);
			IFCResponseBean response = client.sendRequest(requestBean);
			
			IFCDirectory directory = (IFCDirectory)response.getResult();
			System.out.println(directory.getDirPath());	// 目录路径
			System.out.println(directory.getCreateDate());// 创建时间
			System.out.println(directory.getLastAccessTime());// 最后访问时间
			System.out.println(directory.getSize());// 大小
			System.out.println(directory.getLastModifiedTime());// 最后编辑时间
			System.out.println(directory.getIsSymbolicLink());// 是否是符号连接（文件快捷方式）
			System.out.println(directory.getIsDirectory());// 是否目录
			System.out.println(directory.getIsRegularFile());//是否是普通文件
			System.out.println(directory.getIsHidden());// 是否隐藏
			System.out.println(directory.getIsExists());// 是否存在
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
