package cn.com.infcn.monitor.test;

import java.util.Arrays;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import cn.com.infcn.monitor.bean.IFCRequestBean;
import cn.com.infcn.monitor.bean.IFCResponseBean;
import cn.com.infcn.monitor.client.MonitorClientSync;
import cn.com.infcn.monitor.client.ResponseListener;
import cn.com.infcn.monitor.jvm.bean.IFCJVMClassLoading;
import cn.com.infcn.monitor.jvm.bean.IFCJVMCompilation;
import cn.com.infcn.monitor.jvm.bean.IFCJVMGarbageCollector;
import cn.com.infcn.monitor.jvm.bean.IFCJVMMemory;
import cn.com.infcn.monitor.jvm.bean.IFCJVMMemoryManager;
import cn.com.infcn.monitor.jvm.bean.IFCJVMMemoryPool;
import cn.com.infcn.monitor.jvm.bean.IFCJVMMemoryUsage;
import cn.com.infcn.monitor.jvm.bean.IFCJVMOperatingSystem;
import cn.com.infcn.monitor.jvm.bean.IFCJVMRuntime;
import cn.com.infcn.monitor.jvm.bean.IFCJVMThread;
import cn.com.infcn.monitor.jvm.bean.IFCJVMThreadInfo;
import cn.com.infcn.monitor.util.MonitorType;


public class MonitorJVMClientSyncTest {

	static MonitorClientSync client;


	static class MockListener implements ResponseListener{

		public void messageReceived(IFCResponseBean response) {
			
		}
		public void onExceptionCaught(Throwable throwable) {
		}

		public void sessionOpened() {
		}

		public void sessionClosed() {
		}
		
	}

	@BeforeClass
	public static void setUp() throws Exception {
		MockListener listener = new MockListener();
		client = new MonitorClientSync(listener);
		client.connect("192.168.10.98", 56788);
//		client.connect("192.168.3.116", 56788);
	}

	@AfterClass
	public static void tearDown() throws Exception {
		client.disconnect();
	}
	
	public static void main(String[] args) throws Exception{
		MonitorJVMClientSyncTest t = new MonitorJVMClientSyncTest();
		setUp();
//		t.jvmClassLoadingTest();
//		t.jvmCompilation();
//		t.jvmGarbageCollector();
//		t.jvmMemoryManager();
//		t.jvmMemoryPool();
//		t.jvmMemory();
//		t.jvmOperatingSystem();
//		t.jvmRuntime();
		t.jvmThread();
		tearDown();
	}
	

	@Test
	public void jvmClassLoadingTest(){
		try {
			IFCRequestBean requestBean = new IFCRequestBean();
			requestBean.setMonitorType(MonitorType.JVMCLASSLOADING);
			IFCResponseBean result = client.sendRequest(requestBean);

			IFCJVMClassLoading ifcClassLoading = (IFCJVMClassLoading)result.getResult();
			//返回当前加载到 Java 虚拟机中的类的数量。
			System.out.println("已加装当前类："+ifcClassLoading.getLoadedClassCount());
			//返回自 Java 虚拟机开始执行到目前已经加载的类的总数。
			System.out.println("已加载类总数："+ifcClassLoading.getTotalLoadedClassCount());
			//返回自 Java 虚拟机开始执行到目前已经卸载的类的总数。
			System.out.println("已卸御类总数："+ifcClassLoading.getUnloadedClassCount());
			//测试是否已为类加载系统启用了 verbose 输出。
			System.out.println("JVM是否打印GC："+ifcClassLoading.getIsVerbose());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void jvmCompilation(){
		IFCRequestBean requestBean = new IFCRequestBean();
		requestBean.setMonitorType(MonitorType.JVMCOMPILATION);
		IFCResponseBean result = client.sendRequest(requestBean);
		
		IFCJVMCompilation jvmCompilation = (IFCJVMCompilation)result.getResult();

        //返回即时 (JIT) 编译器的名称。
		System.out.println("JIT 编译器:"+jvmCompilation.getName());
		//返回在编译上花费的累积耗费时间的近似值（以毫秒为单位）。
		System.out.println("总编译时间:"+jvmCompilation.getTotalCompilationTime());
		//测试 Java 虚拟机是否支持监视编译时间。 
		System.out.println("否支持监视编译时间:"+jvmCompilation.getIsCompilationTimeMonitoringSupported());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void jvmGarbageCollector(){
		IFCRequestBean requestBean = new IFCRequestBean();
		requestBean.setMonitorType(MonitorType.JVMGARBAGECOLLECTOR);
		IFCResponseBean result = client.sendRequest(requestBean);
		
		List<IFCJVMGarbageCollector> jvmGCs = (List<IFCJVMGarbageCollector>)result.getResult();
		for (IFCJVMGarbageCollector jvmGC : jvmGCs) {
//			System.out.println(jvmGC.getName());
//			//返回已发生的回收的总次数。
//			System.out.println(jvmGC.getCollectionCount());
//			//返回近似的累积回收时间（以毫秒为单位）。 
//			System.out.println(jvmGC.getCollectionTime());
			
			System.out.println("垃圾收集器： 名称='"+jvmGC.getName()+"',收集="+jvmGC.getCollectionCount()+",总花费时间="+jvmGC.getCollectionTime());
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void jvmMemoryManager(){
		IFCRequestBean requestBean = new IFCRequestBean();
		requestBean.setMonitorType(MonitorType.JVMMEMORYMANAGER);
		IFCResponseBean result = client.sendRequest(requestBean);
		
		List<IFCJVMMemoryManager> jvmMemoryManagers = (List<IFCJVMMemoryManager>)result.getResult();
		for (IFCJVMMemoryManager jvmMemoryManager : jvmMemoryManagers) {
			
			//返回此内存管理器管理的内存池名称。
			String[] poolNames = jvmMemoryManager.getMemoryPoolNames();
			System.out.println(Arrays.toString(poolNames));
			//返回表示此内存管理器的名称。
			System.out.println("内存管理器的名称:"+jvmMemoryManager.getName());
		}
	}
	
	@Test
	public void jvmMemoryPool(){
		IFCRequestBean requestBean = new IFCRequestBean();
		requestBean.setMonitorType(MonitorType.JVMMEMORYPOOL);
		IFCResponseBean result = client.sendRequest(requestBean);
		
		IFCJVMMemoryPool jvmMemoryPool = (IFCJVMMemoryPool)result.getResult();
		
		IFCJVMMemoryUsage codeCacheMemoryUsage = jvmMemoryPool.getCodeCacheMemoryUsage();
		System.out.println("---------------Code Cache Memory Usage---------------");
		printMemoryUsage(codeCacheMemoryUsage);
		
		IFCJVMMemoryUsage edenMemoryUsage = jvmMemoryPool.getEdenMemoryUsage();
		System.out.println("---------------PS Eden Space Memory Usage---------------");
		printMemoryUsage(edenMemoryUsage);
		
		IFCJVMMemoryUsage survivorMemoryUsage = jvmMemoryPool.getSurvivorMemoryUsage();
		System.out.println("---------------PS Survivor Space Memory Usage---------------");
		printMemoryUsage(survivorMemoryUsage);
		
		IFCJVMMemoryUsage permGenMemoryUsage = jvmMemoryPool.getPermGenMemoryUsage();
		System.out.println("---------------PS Perm Gen Memory Usage---------------");
		printMemoryUsage(permGenMemoryUsage);
		
		IFCJVMMemoryUsage oldGenMemoryUsage = jvmMemoryPool.getOldGenMemoryUsage();
		System.out.println("---------------PS Old Gen Memory Usage---------------");
		printMemoryUsage(oldGenMemoryUsage);
	}
	
	@Test
	public void jvmMemory(){
		IFCRequestBean requestBean = new IFCRequestBean();
		requestBean.setMonitorType(MonitorType.JVMMEMORY);
		IFCResponseBean result = client.sendRequest(requestBean);
		
		IFCJVMMemory jvmMemory = (IFCJVMMemory)result.getResult();
		
		//返回用于对象分配的堆的当前内存使用量。
		IFCJVMMemoryUsage jvmHeapMemoryUsage = jvmMemory.getHeapMemoryUsage();
		System.out.println("---------------Heap Memory Memory Usage---------------");
		printMemoryUsage(jvmHeapMemoryUsage);
		//返回 Java 虚拟机使用的非堆内存的当前内存使用量。
		IFCJVMMemoryUsage jvmNonMemoryUsage = jvmMemory.getNonHeapMemoryUsage();
		System.out.println("---------------NonHeap Memory Memory Usage---------------");
		printMemoryUsage(jvmNonMemoryUsage);
		
		//返回其终止被挂起的对象的近似数目。
		System.out.println(jvmMemory.getObjectPendingFinalizationCount());
		//测试内存系统的 verbose 输出是否已启用
		System.out.println(jvmMemory.getIsVerbose());
		
	}
	
	private void printMemoryUsage(IFCJVMMemoryUsage jvmMemoryUsage){
		if(jvmMemoryUsage!=null){
			// 返回已提交给 Java 虚拟机使用的内存量（以字节为单位）。 
			System.out.println("Committed:"+jvmMemoryUsage.getCommitted());
			//返回 Java 虚拟机最初从操作系统请求用于内存管理的内存量（以字节为单位）。 
			System.out.println("Init:"+jvmMemoryUsage.getInit());
			//  返回可以用于内存管理的最大内存量（以字节为单位）。 
			System.out.println("Max:"+jvmMemoryUsage.getMax());
			//  返回已使用的内存量（以字节为单位）。 
			System.out.println("Used:"+jvmMemoryUsage.getUsed());
		}
	}
	
	@Test
	public void jvmOperatingSystem(){
		IFCRequestBean requestBean = new IFCRequestBean();
		requestBean.setMonitorType(MonitorType.JVMOPERATINGSYSTEM);
		IFCResponseBean result = client.sendRequest(requestBean);
		
		IFCJVMOperatingSystem jvmOperatingSystem = (IFCJVMOperatingSystem)result.getResult();
		
		// 返回操作系统的架构。
		System.out.println("Arch:"+jvmOperatingSystem.getArch());
		// 返回 Java 虚拟机可以使用的处理器数目。
		System.out.println("AvailableProcessors:"+jvmOperatingSystem.getAvailableProcessors());
		// 返回操作系统名称。
		System.out.println("Name:"+jvmOperatingSystem.getName());
		// 返回最后一分钟内系统加载平均值。
		System.out.println("SystemLoadAverage:"+jvmOperatingSystem.getSystemLoadAverage());
		// 返回操作系统的版本。
		System.out.println("Version:"+jvmOperatingSystem.getVersion());
	}
	
	@Test
	public void jvmRuntime(){
		IFCRequestBean requestBean = new IFCRequestBean();
		requestBean.setMonitorType(MonitorType.JVMRUNTIME);
		IFCResponseBean result = client.sendRequest(requestBean);
		
		IFCJVMRuntime jvmRuntime = (IFCJVMRuntime)result.getResult();
		
		//返回由引导类加载器用于搜索类文件的引导类路径。 
		System.out.println("BootClassPath:"+jvmRuntime.getBootClassPath());
		//返回系统类加载器用于搜索类文件的 Java 类路径。 
		System.out.println("ClassPath:"+jvmRuntime.getClassPath());
		
		//返回传递给 Java 虚拟机的输入变量，其中不包括传递给 main 方法的变量。
		List<String> list = jvmRuntime.getInputArguments();
		System.out.println("-------------JVM初始化参数---begin------------");
		for (String str : list) {
			System.out.println(str);
		}
		System.out.println("-------------JVM初始化参数---end------------");
		//返回 Java 库路径。 
		System.out.println("LibraryPath:"+jvmRuntime.getLibraryPath());
		//返回正在运行的 Java 虚拟机实现的管理接口的规范版本。
		System.out.println("ManagementSpecVersion:"+jvmRuntime.getManagementSpecVersion());
		//返回表示正在运行的 Java 虚拟机的名称。 
		System.out.println("Name:"+jvmRuntime.getName());
		//返回 Java 虚拟机规范名称。 
		System.out.println("SpecName:"+jvmRuntime.getSpecName());
		//返回 Java 虚拟机规范供应商。 
		System.out.println("SpecVendor:"+jvmRuntime.getSpecVendor());
		//返回 Java 虚拟机规范版本。 
		System.out.println("SpecVersion:"+jvmRuntime.getSpecVersion());
		//返回 Java 虚拟机的启动时间（以毫秒为单位）。 
		System.out.println("StartTime:"+jvmRuntime.getStartTime());
		//返回所有系统属性的名称和值的映射。 
		System.out.println("SystemProperties:"+jvmRuntime.getSystemProperties());
		//返回 Java 虚拟机的正常运行时间（以毫秒为单位）。 
		System.out.println("Uptime:"+jvmRuntime.getUptime());
		//返回 Java 虚拟机实现名称。 
		System.out.println("VmName:"+jvmRuntime.getVmName());
		//返回 Java 虚拟机实现供应商。 
		System.out.println("VmVendor:"+jvmRuntime.getVmVendor());
		//返回 Java 虚拟机实现版本。 
		System.out.println("VmVersion:"+jvmRuntime.getVmVersion());
		//测试 Java 虚拟机是否支持由引导类加载器用于搜索类文件的引导类路径机制。
		System.out.println("isBootClassPathSupported:"+jvmRuntime.getIsBootClassPathSupported());

	}
	
	@Test
	public void jvmThread(){
		try {
			IFCRequestBean requestBean = new IFCRequestBean();
			requestBean.setMonitorType(MonitorType.JVMTHREAD);
			IFCResponseBean result = client.sendRequest(requestBean);

			IFCJVMThread jvmThread = (IFCJVMThread)result.getResult();
			//返回当前线程的总 CPU 时间（以毫微秒为单位）。
			System.out.println("CurrentThreadCpuTime:"+jvmThread.getCurrentThreadCpuTime());
			//返回当前线程在用户模式中执行的 CPU 时间（以毫微秒为单位）。
			System.out.println("CurrentThreadUserTime:"+jvmThread.getCurrentThreadUserTime());
			//返回活动守护线程的当前数目。
			System.out.println("DaemonThreadCount:"+jvmThread.getDaemonThreadCount());
			//返回自从 Java 虚拟机启动或峰值重置以来峰值活动线程计数。
			System.out.println("PeakThreadCount:"+jvmThread.getPeakThreadCount());
			//返回活动线程的当前数目，包括守护线程和非守护线程。
			System.out.println("ThreadCount:"+jvmThread.getThreadCount());
			//返回自从 Java 虚拟机启动以来创建和启动的线程总数目。
			System.out.println("TotalStartedThreadCount:"+jvmThread.getTotalStartedThreadCount());
			//查找因为等待获得对象监视器或可拥有同步器而处于死锁状态的线程循环。
			System.out.println("FindDeadlockedThreads:"+jvmThread.getFindDeadlockedThreads());
			//测试 Java 虚拟机是否支持当前线程的 CPU 时间测量。
			System.out.println("isCurrentThreadCpuTimeSupported:"+jvmThread.getIsCurrentThreadCpuTimeSupported());
			//测试 Java 虚拟机是否支持使用对象监视器的监视。
			System.out.println("isObjectMonitorUsageSupported:"+jvmThread.getIsObjectMonitorUsageSupported());
			//测试 Java 虚拟机是否支持使用 可拥有同步器的监视。
			System.out.println("isSynchronizerUsageSupported:"+jvmThread.getIsSynchronizerUsageSupported());
			//测试是否启用了线程争用监视。
			System.out.println("isThreadContentionMonitoringEnabled:"+jvmThread.getIsThreadContentionMonitoringEnabled());
			//测试 Java 虚拟机是否支持线程争用监视。
			System.out.println("isThreadContentionMonitoringSupported:"+jvmThread.getIsThreadContentionMonitoringSupported());
			//测试是否启用了线程 CPU 时间测量。
			System.out.println("isThreadCpuTimeEnabled:"+jvmThread.getIsThreadCpuTimeEnabled());
			//测试 Java 虚拟机实现是否支持任何线程的 CPU 时间测量。
			System.out.println("isThreadCpuTimeSupported:"+jvmThread.getIsThreadCpuTimeSupported());
			
			long[] ids = jvmThread.getAllThreadIds();
			for (long id : ids) {
				IFCJVMThreadInfo jvmThreadInfo = jvmThread.getThreadInfo(id);
				System.out.println("-----------------------");
				//返回与此 ThreadInfo 关联的线程的名称。
				System.out.println("线程名称："+jvmThreadInfo.getThreadName());
				//返回与此 ThreadInfo 关联的线程被阻塞进入或重进入监视器的总次数。
				System.out.println("BlockedCount:"+jvmThreadInfo.getBlockedCount());
				//返回自从启用线程争用监视以来，与此 ThreadInfo 关联的线程被阻塞进入或重进入监视器的近似累计时间（以毫秒为单位）。
				System.out.println("BlockedTime:"+jvmThreadInfo.getBlockedTime());
				//返回对象的字符串表示形式，与此 ThreadInfo 关联的线程被锁定并等待该对象。
				System.out.println("LockName:"+jvmThreadInfo.getLockName());
				//返回拥有对象的线程的 ID，与此 ThreadInfo 关联的线程被阻塞并等待该对象。
				System.out.println("LockOwnerId:"+jvmThreadInfo.getLockOwnerId());
				//返回拥有对象的线程的名称，与此 ThreadInfo 关联的线程被阻塞并等待该对象。
				System.out.println("LockOwnerName:"+jvmThreadInfo.getLockOwnerName());
				//返回与此 ThreadInfo 关联的线程的 ID。
				System.out.println("ThreadId:"+jvmThreadInfo.getThreadId());
				//返回与此 ThreadInfo 关联的线程等待通知的总次数。
				System.out.println("WaitedCount:"+jvmThreadInfo.getWaitedCount());
				//返回自从启用线程争用监视以来,与此 ThreadInfo 关联的线程等待通知的近似累计时间（以毫秒为单位）。
				System.out.println("WaitedTime:"+jvmThreadInfo.getWaitedTime());
				//返回与此 ThreadInfo 关联的线程的状态。
				System.out.println("ThreadState:"+jvmThreadInfo.getThreadState());
				//返回指定 ID 的线程的总 CPU 时间（以毫微秒为单位）。
				System.out.println("ThreadCpuTime:"+jvmThreadInfo.getThreadCpuTime());
				//返回指定 ID 的线程在用户模式中执行的 CPU 时间（以毫微秒为单位）。
				System.out.println("ThreadUserTime:"+jvmThreadInfo.getThreadUserTime());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
